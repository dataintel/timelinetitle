import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { TimelinetitleModule } from '../../../index';
import { AppComponent } from './app.component';
import { SlimScrollModule } from 'ng2-slimscroll';
import { Timelinetitle1Component } from './timelinetitle1/timelinetitle1.component';

@NgModule({
  declarations: [
    AppComponent,
    Timelinetitle1Component
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    TimelinetitleModule,
    SlimScrollModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
