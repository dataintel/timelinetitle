import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Timelinetitle1Component } from './timelinetitle1.component';

describe('Timelinetitle1Component', () => {
  let component: Timelinetitle1Component;
  let fixture: ComponentFixture<Timelinetitle1Component>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Timelinetitle1Component ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Timelinetitle1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
