import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BaseWidgetTimelineTitleComponent } from './src/timelinetitle.component';
import { SlimScrollModule } from 'ng2-slimscroll';

export * from './src/timelinetitle.component';


@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    BaseWidgetTimelineTitleComponent,
  ],
  exports: [
    BaseWidgetTimelineTitleComponent,

  ]
})
export class TimelinetitleModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: TimelinetitleModule,
      providers: []
    };
  }
}
